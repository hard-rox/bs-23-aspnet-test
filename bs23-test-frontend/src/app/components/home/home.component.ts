import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagedList } from 'src/app/types/paged-list';
import { Post } from 'src/app/types/post';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor(private http: HttpClient) { }

  searchText: string = "";
  pagedPosts = new PagedList<Post>();
  pages: number[];

  ngOnInit(): void {
    this.onLoadPage(1);
  }

  onLoadPage(pageNumber: number) {
    this.http.get<PagedList<Post>>("https://localhost:44308/api/Posts?"+"searchString="+ this.searchText +"&PageNumber=" + pageNumber + "&PageSize=2")
      .subscribe(data => {
        console.log(data);
        this.pagedPosts = data;
        this.setPaging();
      })
  }

  onSearchChange(searchValue: string): void {  
    this.searchText = searchValue;
    this.onLoadPage(1);
  }

  private setPaging() {
    //console.log(this.totalPage, this.currentPage);
    if (this.pagedPosts.totalPages <= 1) {
      this.pages = [1];
      return;
    }
    if (this.pagedPosts.currentPage === 1) {
      this.pages = [this.pagedPosts.currentPage, this.pagedPosts.currentPage + 1];
      return;
    }
    else if (this.pagedPosts.currentPage === this.pagedPosts.totalPages) {
      this.pages = [this.pagedPosts.currentPage - 1, this.pagedPosts.currentPage];
      return;
    }
    if (this.pagedPosts.totalPages > 2) {
      this.pages = [this.pagedPosts.currentPage - 1, this.pagedPosts.currentPage, this.pagedPosts.currentPage + 1];
    }
    else {
      this.pages = [1, 2];
      return;
    }
  }

}
