import { Comment } from "./comment";

export class Post {
    id: number;
    text: string;
    user: string;
    createdAt: Date;
    comments: Comment[];
}