export class Comment {
    id: number;
    commentText: string;
    user: string;
    likeCount: number;
    dislikeCount: number;
    createdAt: Date;
}