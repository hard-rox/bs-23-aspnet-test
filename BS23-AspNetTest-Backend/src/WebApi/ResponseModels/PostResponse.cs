﻿using System;
using System.Collections.Generic;

namespace WebApi.ResponseModels
{
    public class PostResponse
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string User { get; set; }
        public DateTime CreatedAt { get; set; }

        public ICollection<CommentResponse> Comments { get; set; }
    }
}
