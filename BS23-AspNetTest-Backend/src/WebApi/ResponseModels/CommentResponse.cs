﻿using System;

namespace WebApi.ResponseModels
{
    public class CommentResponse
    {
        public int Id { get; set; }
        public string CommentText { get; set; }
        public string User { get; set; }
        public int LikeCount { get; set; }
        public int DislikeCount { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
