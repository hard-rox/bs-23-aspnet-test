﻿using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebApi.Helpers;
using WebApi.ResponseModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPostService _postService;

        public PostsController(IPostService postService, IMapper mapper)
        {
            _postService = postService;
            _mapper = mapper;
        }

        // GET: api/<PostsController>
        [HttpGet]
        public async Task<PagedList<PostResponse>> Get([FromQuery] string searchString, [FromQuery] PaginationParameter pagination)
        {
            var queryable = _postService.Get();
            if (!string.IsNullOrEmpty(searchString))
            {
                queryable = queryable.Where(p => p.Text.Contains(searchString)).AsQueryable();
            }
            queryable = queryable.Include(p => p.Comments);

            var postsPagedList = await PagedList<Post>.ToPagedListAsync(queryable, pagination);
            var responsePagedList = _mapper.Map<PagedList<PostResponse>>(postsPagedList);
            return responsePagedList;
        }
    }
}
