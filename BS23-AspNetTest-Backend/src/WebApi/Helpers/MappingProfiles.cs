﻿using AutoMapper;
using Domain;
using WebApi.ResponseModels;

namespace WebApi.Helpers
{
    internal class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Comment, CommentResponse>();
            CreateMap<Post, PostResponse>();
            CreateMap<PagedList<Post>, PagedList<PostResponse>>();
        }
    }
}
