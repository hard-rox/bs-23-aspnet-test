﻿namespace WebApi.Helpers
{
    public class PaginationParameter
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public PaginationParameter()
        {
            PageNumber = 1;
            PageSize = 0;
        }
    }
}