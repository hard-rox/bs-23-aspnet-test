﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Helpers
{
    public class PagedList<T>
    {
        public List<T> Data { get; private set; }
        public int CurrentPage { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }
        public bool HasPrevious => CurrentPage > 1;
        public bool HasNext => CurrentPage < TotalPages;

        public PagedList(){}

        public PagedList(List<T> items, int count, int pageNumber, int pageSize)
        {
            TotalCount = count;
            PageSize = pageSize == 0 ? count : pageSize;
            CurrentPage = pageNumber;
            TotalPages = pageSize == 0 ? 1 : (int)Math.Ceiling(count / (double)pageSize);
            Data = items;
        }
        public static async Task<PagedList<T>> ToPagedListAsync(IQueryable<T> source, PaginationParameter pagination)
        {
            var count = await source.CountAsync();
            if (pagination.PageSize == 0)
                pagination.PageSize = 5;
            
            var items = await source.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
            return new PagedList<T>(items, count, pagination.PageNumber, pagination.PageSize);
        }
    }
}
