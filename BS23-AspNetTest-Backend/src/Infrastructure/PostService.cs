﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain;

namespace Infrastructure
{
    internal class PostService : IPostService
    {
        private readonly IApplicationDbContext _context;

        public PostService(IApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Post> Get()
        {
            #region Insert Dummy Data

            //_context.Posts.AddRange(
            //    new Post()
            //    {
            //        Text = "Post 1",
            //        User = "Admin",
            //        CreatedAt = DateTime.UtcNow,
            //        Comments = new List<Comment>()
            //        {
            //            new Comment()
            //            {
            //                CommentText = "Comment1",
            //                User = "User1",
            //                CreatedAt = DateTime.UtcNow,
            //                LikeCount = 268,
            //                DislikeCount = 126
            //            },
            //            new Comment()
            //            {
            //                CommentText = "Comment2",
            //                User = "User2",
            //                CreatedAt = DateTime.UtcNow,
            //                LikeCount = 268,
            //                DislikeCount = 126
            //            },
            //            new Comment()
            //            {
            //                CommentText = "Comment3",
            //                User = "User3",
            //                CreatedAt = DateTime.UtcNow,
            //                LikeCount = 268,
            //                DislikeCount = 126
            //            },
            //        }
            //    },
            //    new Post()
            //    {
            //        Text = "Post 2",
            //        User = "Admin",
            //        CreatedAt = DateTime.UtcNow,
            //        Comments = new List<Comment>()
            //        {
            //            new Comment()
            //            {
            //                CommentText = "Comment1",
            //                User = "User1",
            //                CreatedAt = DateTime.UtcNow,
            //                LikeCount = 268,
            //                DislikeCount = 126
            //            },
            //            new Comment()
            //            {
            //                CommentText = "Comment2",
            //                User = "User2",
            //                CreatedAt = DateTime.UtcNow,
            //                LikeCount = 268,
            //                DislikeCount = 126
            //            },
            //            new Comment()
            //            {
            //                CommentText = "Comment3",
            //                User = "User3",
            //                CreatedAt = DateTime.UtcNow,
            //                LikeCount = 268,
            //                DislikeCount = 126
            //            },
            //        }
            //    },
            //    new Post()
            //    {
            //        Text = "Post 3",
            //        User = "Admin",
            //        CreatedAt = DateTime.UtcNow
            //    }
            //);
            //Task.Run(() => _context.SaveChangesAsync());

            #endregion

            return _context.Posts;
        }
    }
}
