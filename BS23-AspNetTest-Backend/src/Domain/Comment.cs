﻿namespace Domain
{
    public class Comment : AuditableEntity
    {
        public int Id { get; set; }
        public string CommentText { get; set; }
        public string User { get; set; }
        public int LikeCount { get; set; }
        public int DislikeCount { get; set; }

        public int PostId { get; set; }
        public virtual Post Post { get; set; }
    }
}
