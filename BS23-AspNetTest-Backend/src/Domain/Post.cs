﻿using System.Collections.Generic;

namespace Domain
{
    public class Post : AuditableEntity
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string User { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}
