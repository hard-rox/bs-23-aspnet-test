﻿using System;

namespace Domain
{
    public class AuditableEntity
    {
        public Guid EntityGuid { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastModifiedAt { get; set; }
    }
}
