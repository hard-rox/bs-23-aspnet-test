﻿using System.Linq;
using Domain;

namespace Application.Interfaces
{
    public interface IPostService
    {
        IQueryable<Post> Get();
    }
}
